# -------------------------------------------------------------------
# Variables
# -------------------------------------------------------------------

export GOPATH="$HOME/gocode"

# PATH
export PATH="/usr/lib/ccache/bin:$GOPATH/bin:$HOME/bin:$HOME/.mutt/bin:$PATH"

# Disable Virtualenv Prompt expansion
export VIRTUAL_ENV_DISABLE_PROMPT=1

export WORKON_HOME=$HOME/.virtualenv
export PROJECT_HOME=$HOME/Development

export LC_COLLATE="de_DE.UTF-8"
export LC_CTYPE="de_DE.UTF-8"
export LANG="en_US.UTF-8"

export ZSH_DISABLE_COMPFIX=true

export COLORTERM=truecolor

export EDITOR=vim
