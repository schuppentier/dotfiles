export ZPLUG_HOME=~/.zplug
source $ZPLUG_HOME/init.zsh

zplug "isqua/bureau", as:command
zplug "zsh-users/zsh-history-substring-search"
zplug "zsh-users/zsh-completions"

zplug load

