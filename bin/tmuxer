#!/usr/bin/env zsh

HOSTNAME="$(hostname -s)"
SESSION_NAME="default"
CMD_CLOCK="tty-clock -BcC 4"
CMD_CAL="watch -n 900 'cal'"
CMD_SSH_TURING="mosh turing"
CMD_SSH_TESLA="mosh tesla"
CMD_HTOP="htop"
CMD_WEGO="watch -ctn 900 'wego'"

export SSH_AUTH_SOCK=$(launchctl getenv SSH_AUTH_SOCK)

if [ ! $(tmux has-session -t $SESSION_NAME) ]
then
	#####################################
	# Initially create session          #
	#####################################
	tmux new-session -s $SESSION_NAME -n $HOSTNAME -d

	#####################################
	# First Window, with pane foo       #
	#####################################
	tmux split-window -h -t $SESSION_NAME:$HOSTNAME -p 85 # Split for utilities pane
	tmux split-window -v -t $SESSION_NAME:$HOSTNAME.1 -p 50 # Split utilities pane itself

	tmux select-pane -t 1
	sleep 1
	tmux send-keys					-t $SESSION_NAME "$CMD_CLOCK" C-m

	tmux select-pane -t 3
	tmux send-keys					-t $SESSION_NAME "$CMD_CAL" C-m

	#####################################
	# Other Windows                     #
	#####################################
	tmux new-window -an Turing		-t $SESSION_NAME "$CMD_SSH_TURING"
	tmux new-window -an Tesla		-t $SESSION_NAME "$CMD_SSH_TESLA"
	tmux new-window -an htop		-t $SESSION_NAME "$CMD_HTOP"
	#tmux new-window -an Wetter		-t $SESSION_NAME "$CMD_WEGO"

	#####################################
	# Attach to server                  #
	#####################################
	tmux select-window -t $SESSION_NAME:$HOSTNAME
	tmux select-pane -t 2
fi

exec tmux attach -t $SESSION_NAME
